package server.components;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Vector;

import chat.utils.ProtocolDictionary;
import server.exceptions.KeyWordNoExistsException;
import server.exceptions.ProtocolErrorException;


public class ConnectedUser {
	private String userName;
	private Socket assocSocket;
	private DataOutputStream output;
	private DataInputStream input;
	private boolean logged;
	private List<ReceiverClass> receivers;
	private ChatProtocoleable protocol;
	
	private boolean sendingMessage;
	private boolean unlogging;
	private boolean onlyOneMessage;
	
	private static final int TIMEOUT_INTERNAL_RECEIVER = 5000;
	private static final int INTERNAL_RECEIVERS = 5;
	
	public ConnectedUser(Socket assocSocket, String userName) throws IOException {
		this.assocSocket = assocSocket;
		logged = true;
		receivers = new Vector<ReceiverClass>();
		output = new DataOutputStream(assocSocket.getOutputStream());
		input = new DataInputStream(assocSocket.getInputStream());
		this.userName = userName;
		sendingMessage = false;
		unlogging = false;
		onlyOneMessage = false;
	}
	public void setChatProtocoleable(ChatProtocoleable protocol) {
		this.protocol = protocol;
	}
	
	public void logout() {
		if (!unlogging) {
			unlogging = true;
			try {
				output.close();
				input.close();
				assocSocket.close();
				/*while (unlogging)
					synchronized(this) {
						wait();
					}*/
			} catch (IOException e) {
				e.printStackTrace();
			}/* catch (InterruptedException e) {
				e.printStackTrace();
			}*/
			logged = false;
		}
	}
	public String getUserName() {
		return userName;
	}
	public void start() throws SocketException {
		for (int i = 0; i < INTERNAL_RECEIVERS; i++) {
			ReceiverClass receiver = new ReceiverClass();
			receiver.start();
			receivers.add(receiver);
		}
	}
	private void receiveMessage() throws IOException {
		synchronized (this) {
			String readed = input.readUTF();
			sendingMessage = true;
			sendMessage(protocol.analyzeMessage(readed, userName));
			sendingMessage = false;
			notifyAll();
		}
	}
	private void sendMessage(String message) throws IOException {
		//synchronized (output) {
			output.writeUTF(message);
		//}
		
	}
	public void send(String message) {
		SenderClass sender = new SenderClass(message);
		sender.start();
	}
	private class SenderClass extends Thread {
		private String message;
		public SenderClass(String message) {
			this.message = message;
		}
		@Override
		public void run() {
			super.run();
			try {
				while (sendingMessage) {
					synchronized(this) {
						wait();
					}
				}
				sendMessage(message);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	private class ReceiverClass extends Thread {
		
		@Override
		public synchronized void start() {
			super.start();
			setPriority(MAX_PRIORITY);
		}

		@Override
		public void run() {
			super.run();
			while (logged) {
				try {
					receiveMessage();
				} catch (/*SocketException |*/ IOException e) {
					if (!onlyOneMessage) {
						onlyOneMessage = true;
						protocol.analyzeMessage("logout", userName);
						synchronized(ConnectedUser.this) {
							ConnectedUser.this.notifyAll();
						}
					}
				}/* catch (IOException e) {
					e.printStackTrace();
				}*/
			}
		}
	}
}
