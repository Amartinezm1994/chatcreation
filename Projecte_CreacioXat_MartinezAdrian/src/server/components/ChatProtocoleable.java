package server.components;
import server.exceptions.ProtocolErrorException;


public interface ChatProtocoleable {
	public String analyzeMessage(String message, String userName);
}
