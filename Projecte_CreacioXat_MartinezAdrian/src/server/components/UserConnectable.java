package server.components;
import server.exceptions.RepeatedUserException;


public interface UserConnectable {
	public void connectUser(String nameUser, ConnectedUser user) throws RepeatedUserException;
}
