package server.components;

public interface AccepteableSocket {
	public void acceptClient();
	public boolean continueAccept();
}
