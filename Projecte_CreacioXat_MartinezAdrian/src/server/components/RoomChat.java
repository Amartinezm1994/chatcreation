package server.components;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import chat.utils.Dictionable;
import server.core.ServerChat;


public class RoomChat {
	private List<ConnectedUser> connectedUsers;
	private String roomName;
	private Dictionable dictionary;
	
	public RoomChat(String roomName, Dictionable dictionary) {
		connectedUsers = new Vector<ConnectedUser>();
		this.roomName = roomName;
		this.dictionary = dictionary;
	}
	public String getUsersAsString() {
		String users = connectedUsers.get(0).getUserName();
		String separator = dictionary.getInternalSeparator();
		for (int i = 1; i < connectedUsers.size(); i++) {
			users += separator + connectedUsers.get(i).getUserName();
		}
		return users;
	}
	public void sendMessage(String userName, String message) {
		String internalSeparator = dictionary.getInternalSeparator();
		String messageProtocolized = roomName + internalSeparator 
				+ userName + internalSeparator + message;
		for (ConnectedUser cu : connectedUsers) {
			if (!cu.getUserName().equals(userName))
				cu.send(messageProtocolized);
		}
	}
	public String getRoomName() {
		return roomName;
	}
	public boolean addUserToRoom(ConnectedUser user) {
		for (ConnectedUser cu : connectedUsers) {
			if (cu.equals(user)) {
				return false;
			}
		}
		connectedUsers.add(user);
		return true;
	}
	public boolean removeUserOfRoom(String user) {
		Iterator<ConnectedUser> it = connectedUsers.iterator();
		while (it.hasNext()) {
			ConnectedUser us = it.next();
			if (us.getUserName().equals(user)) {
				it.remove();
				return true;
			}
		}
		return false;
	}
	public boolean isUserInChat(String userName) {
		for (ConnectedUser cu : connectedUsers) {
			if (cu.getUserName().equals(userName)) {
				return true;
			}
		}
		return false;
	}
	public boolean isEmptyChat() {
		return connectedUsers.isEmpty();
	}
}
