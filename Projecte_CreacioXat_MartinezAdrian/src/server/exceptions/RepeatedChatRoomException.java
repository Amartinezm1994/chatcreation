package server.exceptions;

public class RepeatedChatRoomException extends Exception {
	public RepeatedChatRoomException(String msg) {
		super(msg);
	}
	public RepeatedChatRoomException() {
	}
}
