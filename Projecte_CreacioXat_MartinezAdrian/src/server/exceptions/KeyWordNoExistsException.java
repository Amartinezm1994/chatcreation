package server.exceptions;

public class KeyWordNoExistsException extends Exception {
	public KeyWordNoExistsException(String msg) {
		super(msg);
	}

	public KeyWordNoExistsException() {
	}
}
