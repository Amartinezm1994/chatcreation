package server.exceptions;

public class MalformedDictionaryException extends Exception {
	public MalformedDictionaryException(String msg) {
		super(msg);
	}
}
