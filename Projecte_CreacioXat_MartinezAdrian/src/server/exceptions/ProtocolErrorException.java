package server.exceptions;

public class ProtocolErrorException extends Exception {
	public ProtocolErrorException(String msg) {
		super(msg);
	}
	public ProtocolErrorException() {
	}
}
