package server.exceptions;

public class RepeatedUserException extends Exception {
	public RepeatedUserException(String msg) {
		super(msg);
	}
}
