package server.core;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import chat.utils.Dictionable;
import chat.utils.ProtocolDictionary;
import server.components.AccepteableSocket;
import server.components.ChatProtocoleable;
import server.components.ConnectedUser;
import server.components.RoomChat;
import server.components.UserConnectable;
import server.exceptions.KeyWordNoExistsException;
import server.exceptions.MalformedDictionaryException;
import server.exceptions.ProtocolErrorException;
import server.exceptions.RepeatedUserException;


public class ServerChat implements AccepteableSocket, UserConnectable, ChatProtocoleable, Dictionable {
	
	private ServerSocket serverSock;
	private int welcomePort;
	private boolean running;
	private Map<String, ConnectedUser> users;
	private Map<String, RoomChat> rooms;
	private int timeoutRefreshListen;
	private List<MultipleListen> listeners;
	private ProtocolDictionary dictionary;
	
	public static final int DEFAULT_REFRESH_TIME = 5000;
	private static final int INTERNAL_LISTENERS = 5;
	public static final String SEPARATOR_USER_INDICATOR = ":";
	
	public ServerChat(int welcomePort, int timeoutRefreshListen) 
			throws IOException, MalformedDictionaryException, NoSuchAlgorithmException {
		this.welcomePort = welcomePort;
		serverSock = new ServerSocket(welcomePort);
		running = false;
		users = new Hashtable<String, ConnectedUser>();
		rooms = new Hashtable<String, RoomChat>();
		listeners = new Vector<MultipleListen>();
		this.timeoutRefreshListen = timeoutRefreshListen;
		dictionary = ProtocolDictionary.getInstance();
	}
	
	public ServerChat(int welcomePort) throws IOException, MalformedDictionaryException, NoSuchAlgorithmException {
		this(welcomePort, DEFAULT_REFRESH_TIME);
	}
	
	public void startServer() throws SocketException {
		running = true;
		serverSock.setSoTimeout(timeoutRefreshListen);
		for (int i = 0; i < INTERNAL_LISTENERS; i++) {
			MultipleListen listener = new MultipleListen(this);
			listener.start();
			listeners.add(listener);
		}
	}
	public void stopServer() {
		running = false;
		for (MultipleListen ml : listeners) {
			try {
				ml.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (ConnectedUser us : users.values()) {
			us.logout();
		}
	}
	@Override
	public boolean continueAccept() {
		return running;
	}
	
	public void acceptClient() {
		try {
			Socket sock = null;
			synchronized (this) {
				sock = serverSock.accept();
			}
			UserLogin login = new UserLogin(sock, this);
			login.start();
		} catch (SocketTimeoutException e) {
			// Do nothing, we aren't interested in this
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void connectUser(String nameUser, ConnectedUser user) throws RepeatedUserException {
		if (users.containsKey(nameUser)) {
			String messageToSend = dictionary.getWord(ProtocolDictionary.CODE_ERROR) 
					+ dictionary.getWord(ProtocolDictionary.CODE_SPLIT) + "Repeated user.";
			throw new RepeatedUserException(messageToSend);
		}
		user.setChatProtocoleable(this);
		try {
			user.start();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		users.put(nameUser, user);
		String messageToSend = dictionary.getWord(ProtocolDictionary.CODE_OK) 
				+ dictionary.getWord(ProtocolDictionary.CODE_SPLIT) + "Welcome";
		user.send(messageToSend);
	}
	@Override
	public String analyzeMessage(String allMessage, String userName) {
		String separator = dictionary.getWord(ProtocolDictionary.CODE_SPLIT);
		String[] messages = allMessage.split(separator);
		String messageToSend = "";
		try {
			int key = dictionary.getCode(messages[0]);
			switch(key) {
			case ProtocolDictionary.CODE_CREATEROOM:
				String nameRoom = messages[1];
				if (rooms.containsKey(nameRoom)) {
					messageToSend += "Room already exists";
					throw new ProtocolErrorException();
				}
				RoomChat room = new RoomChat(nameRoom, this);
				room.addUserToRoom(users.get(userName));
				rooms.put(nameRoom, room);
				break;
			case ProtocolDictionary.CODE_GETUSERSROOM:
				String nameRoomGetUsers = messages[1];
				if (!rooms.containsKey(nameRoomGetUsers)) {
					messageToSend += "Innexistent room";
					throw new ProtocolErrorException();
				}
				RoomChat roomGetUsers = rooms.get(nameRoomGetUsers);
				messageToSend += roomGetUsers.getUsersAsString();
				break;
			case ProtocolDictionary.CODE_JOINROOM:
				String nameRoomToJoin = messages[1];
				if (!rooms.containsKey(nameRoomToJoin)) {
					messageToSend += "Innexistent room";
					throw new ProtocolErrorException();
				}
				RoomChat roomToJoin = rooms.get(nameRoomToJoin);
				if (!roomToJoin.addUserToRoom(users.get(userName))) {
					messageToSend += "User already in room";
					throw new ProtocolErrorException();
				}
				roomToJoin.sendMessage(userName, "Connected.");
				break;
			case ProtocolDictionary.CODE_LEAVEROOM:
				String nameRoomToLeave = messages[1];
				if (!rooms.containsKey(nameRoomToLeave)) {
					messageToSend += "Innexistent room";
					throw new ProtocolErrorException();
				}
				RoomChat roomToLeave = rooms.get(nameRoomToLeave);
				roomToLeave.removeUserOfRoom(userName);
				if (roomToLeave.isEmptyChat()) {
					rooms.remove(nameRoomToLeave);
				} else {
					roomToLeave.sendMessage(userName, "Has been disconnected.");
				}
				break;
			case ProtocolDictionary.CODE_SENDMESSAGE:
				String nameRoomToSend = messages[1];
				String messageToSendRoom = messages[2];
				if (!rooms.containsKey(nameRoomToSend)) {
					messageToSend += "Innexistent room";
					throw new ProtocolErrorException();
				}
				//String internalSeparator = dictionary.getWord(ProtocolDictionary.CODE_INTERNALSEPARATOR);
				RoomChat roomToSend = rooms.get(nameRoomToSend);
				roomToSend.sendMessage(userName, messageToSendRoom);
				break;
			case ProtocolDictionary.CODE_LOGOUT:
				for (Entry<String, RoomChat> entry : rooms.entrySet()) {
					RoomChat roomToNotify = entry.getValue();
					if (roomToNotify.isUserInChat(userName)) {
						roomToNotify.sendMessage(userName, "Disconnected.");
						roomToNotify.removeUserOfRoom(userName);
						if (roomToNotify.isEmptyChat()) {
							rooms.remove(roomToNotify.getRoomName());
						}
					}
				}
				ConnectedUser userConn = users.get(userName);
				userConn.logout();
				users.remove(userName);
				break;
			case ProtocolDictionary.CODE_GETROOMS:
				String internalSeparatorRooms = 
					dictionary.getWord(ProtocolDictionary.CODE_INTERNALSEPARATOR);
				if (rooms.isEmpty()) {
					messageToSend += "No have rooms.";
					throw new ProtocolErrorException();
				}
				for (String roomName : rooms.keySet()) {
					messageToSend += roomName + internalSeparatorRooms;
				}
				break;
			}
		} catch (KeyWordNoExistsException | IndexOutOfBoundsException | ProtocolErrorException e) {
			return dictionary.getWord(ProtocolDictionary.CODE_ERROR) + separator + messageToSend;
		}
		return dictionary.getWord(ProtocolDictionary.CODE_OK) + separator + messageToSend;
	}
	
	@Override
	public String getInternalSeparator() {
		return dictionary.getWord(ProtocolDictionary.CODE_INTERNALSEPARATOR);
	}
}
