package server.core;
import server.components.AccepteableSocket;


public class MultipleListen extends Thread {
	
	private AccepteableSocket accepteable;
	
	public MultipleListen(AccepteableSocket accepteable) {
		this.accepteable = accepteable;
	}

	@Override
	public void run() {
		super.run();
		while (accepteable.continueAccept()) {
			accepteable.acceptClient();
		}
	}
}
