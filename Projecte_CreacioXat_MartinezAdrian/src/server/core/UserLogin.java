package server.core;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import server.components.ConnectedUser;
import server.components.UserConnectable;
import server.exceptions.RepeatedUserException;

public class UserLogin extends Thread {
	
	private UserConnectable connectable;
	private Socket userSocket;
	//private ChatProtocoleable protocol;
	
	public UserLogin(Socket userSocket, UserConnectable connectable) {
		this.connectable = connectable;
		this.userSocket = userSocket;
		//this.protocol = protocol;
	}
	
	@Override
	public void run() {
		super.run();
		try {
			InputStream inputUser = userSocket.getInputStream();
			DataInputStream dataEntry = new DataInputStream(inputUser);
			String userName = dataEntry.readUTF();
			ConnectedUser user = new ConnectedUser(userSocket, userName);
			try {
				connectable.connectUser(userName, user);
			} catch (RepeatedUserException e) {
				OutputStream outUser = userSocket.getOutputStream();
				DataOutputStream dataOut = new DataOutputStream(outUser);
				dataOut.writeUTF(e.getMessage());
				dataEntry.close();
				dataOut.close();
				userSocket.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
