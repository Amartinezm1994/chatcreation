package server.launcher;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import server.core.ServerChat;
import server.exceptions.MalformedDictionaryException;


public class Launcher {
	
	public static final int WELCOME_PORT = 6000;
	
	public static void main(String[] args) throws NoSuchAlgorithmException, IOException, MalformedDictionaryException {
		ServerChat server = new ServerChat(WELCOME_PORT);
		server.startServer();
	}
}
