package chat.utils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import server.exceptions.KeyWordNoExistsException;
import server.exceptions.MalformedDictionaryException;


public class ProtocolDictionary {
	
	public static final String FILE_PATH = "./dictionary/words";
	public static final String SEPARATOR = ":";
	private static final String ENCRIPTATION = "MD5";
	
	private Map<Integer, List<String>> dictionary;
	private byte[] md5Checksum;
	
	public static final int CODE_LOGOUT = 1;
	public static final int CODE_CREATEROOM = 2;
	public static final int CODE_JOINROOM = 3;
	public static final int CODE_LEAVEROOM = 4;
	public static final int CODE_SENDMESSAGE = 5;
	public static final int CODE_OK = 6;
	public static final int CODE_ERROR = 7;
	public static final int CODE_CONNECT = 8;
	public static final int CODE_GETUSERSROOM = 9;
	public static final int CODE_SPLIT = 10;
	public static final int CODE_GETROOMS = 11;
	public static final int CODE_INTERNALSEPARATOR = 12;
	
	private static ProtocolDictionary instance = null;
	
	private ProtocolDictionary() throws IOException, MalformedDictionaryException, NoSuchAlgorithmException {
		dictionary = new HashMap<Integer, List<String>>();
		try (BufferedReader br = new BufferedReader(new FileReader(FILE_PATH))) {
			String phrase = null;
			while ((phrase = br.readLine()) != null) {
				String[] line = phrase.split(SEPARATOR);
				Integer value = null;
				try {
					value = Integer.parseInt(line[0]);
				} catch (NumberFormatException e) {
					throw new MalformedDictionaryException("Error when parse a number.");
				}
				
				List<String> params = new ArrayList<String>();
				for (int i = 1; i < line.length; i++) {
					params.add(line[i]);
				}
				dictionary.put(value, params);
			}
		}
		try (InputStream fis = new FileInputStream(FILE_PATH)) {
			byte[] buffer = new byte[1024];
			MessageDigest comp = MessageDigest.getInstance(ENCRIPTATION);
			int readed;
			while ((readed = fis.read(buffer)) != -1) {
				if (readed > 0) {
					comp.update(buffer, 0, readed);
				}
			}
			md5Checksum = comp.digest();
		}
	}
	
	public static ProtocolDictionary getInstance() throws NoSuchAlgorithmException, IOException, MalformedDictionaryException {
		if (instance == null) {
			instance = new ProtocolDictionary();
		}
		return instance;
	}
	
	public int getCode(String word) throws KeyWordNoExistsException {
		for (Entry<Integer, List<String>> entries : dictionary.entrySet()) {
			String wordGetted = entries.getValue().get(0);
			//System.out.println(wordGetted + " " + word);
			if (word.equals(wordGetted)) {
				return entries.getKey();
			}
		}
		throw new KeyWordNoExistsException();
	}
	public String getWord(int key)/* throws KeyWordNoExistsException*/ {
		/*if (!dictionary.containsKey(key)) {
			throw new KeyWordNoExistsException();
		}*/
		return dictionary.get(key).get(0);
	}
	
	public byte[] getMd5Sum() {
		return md5Checksum;
	}
}
