package client.ui.components;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

import chat.utils.Dictionable;
import client.core.interfaces.Responseable;
import client.ui.interfaces.RoomJoineable;

public class DialogSelectRoom extends JDialog implements Responseable {
	
	private boolean waiting;
	private boolean doAction;
	
	private RoomJoineable joineable;
	private Dictionable dictionary;
	private String responseError;
	private Vector<String> roomsAvailables;
	private JTable tablePanel;
	
	public DialogSelectRoom(RoomJoineable joineable, Dictionable dictionary) {
		waiting = true;
		doAction = false;
		this.joineable = joineable;
		this.dictionary = dictionary;
	}
	
	private void constructDialog(Vector<String> roomsAvailables) {
		this.roomsAvailables = roomsAvailables;
		
		setModal(true);
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		
		Vector<Vector<String>> rowData = new Vector<Vector<String>>();
		Vector<String> header = new Vector<String>();
		header.add("Sales");
		
		for (String r : roomsAvailables) {
			Vector<String> room = new Vector<String>();
			room.add(r);
			rowData.add(room);
		}
		
		tablePanel = new JTable(rowData, header);
		contentPane.add(tablePanel, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		contentPane.add(buttonPanel, BorderLayout.PAGE_END);
		
		buttonPanel.setLayout(new GridLayout(1, 2));
		
		JButton buttonOk = new JButton("Confirmar");
		buttonPanel.add(buttonOk);
		buttonOk.addActionListener(new ActionOk());
		
		JButton buttonCancel = new JButton("Cancel·lar");
		buttonPanel.add(buttonCancel);
		buttonCancel.addActionListener(new ActionCancel());
		
		pack();
		
		setVisible(true);
	}

	@Override
	public void setResponse(boolean state, String response) {
		doAction = state;
		if (state) {
			String[] rooms = response.split(dictionary.getInternalSeparator());
			Vector<String> roomsAvailables = new Vector<String>();
			for (String r : rooms) {
				roomsAvailables.add(r);
			}
			constructDialog(roomsAvailables);
		} else {
			responseError = response;
			JOptionPane.showMessageDialog(null, "No hi han sales creades.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	@Override
	public String getReasonOfThrow() {
		return responseError;
	}

	@Override
	public boolean waiting() {
		return waiting;
	}

	@Override
	public boolean doAction() {
		return doAction;
	}
	
	private class ActionOk implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int selectedRow = tablePanel.getSelectedRow();
			if (selectedRow != -1) {
				joineable.joinRoom(roomsAvailables.get(selectedRow));
				dispose();
			}
		}
		
	}
	
	private class ActionCancel implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
		
	}
}
