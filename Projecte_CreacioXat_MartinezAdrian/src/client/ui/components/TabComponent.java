package client.ui.components;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicButtonUI;

import client.ui.interfaces.TabCloseable;

public class TabComponent extends JPanel {
	
	private TabCloseable closeable;
	private String tabName;
	
	private static final int SIZE_BUTTON = 17;
	
	public TabComponent(TabCloseable closeable, String tabName) {
		setLayout(new FlowLayout());
		this.tabName = tabName;
		this.closeable = closeable;
		setOpaque(false);
		JLabel labelPane = new JLabel(tabName);
		add(labelPane);
		JButton buttonTab = new TabButton();
		add(buttonTab);
	}
	
	private class TabButton extends JButton implements ActionListener {
		public TabButton(/*TabCloseable closeable, String tabName*/) {
		//	this.tabName = tabName;
			setPreferredSize(new Dimension(SIZE_BUTTON, SIZE_BUTTON));
			setToolTipText("Tancar sala");
			setUI(new BasicButtonUI());
			setContentAreaFilled(false);
			setBorder(BorderFactory.createEtchedBorder());
			addActionListener(this);
		}
	
		@Override
		public void actionPerformed(ActionEvent arg0) {
			closeable.closeTab(tabName);
		}
	}
}
