package client.ui.components;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;

import javax.swing.JTabbedPane;

import client.ui.interfaces.CloseableRoom;
import client.ui.interfaces.Sendeable;
import client.ui.interfaces.TabCloseable;
import client.ui.interfaces.UserObtainable;
import client.ui.interfaces.WritteableChat;
import chat.utils.Dictionable;
import server.exceptions.MalformedDictionaryException;

public class RoomTabbedPane extends JTabbedPane implements TabCloseable {
	
	private CloseableRoom roomCloseable;
	private UserObtainable obtainable;
	private Sendeable sender;
	private Dictionable dictionary;
	private Hashtable<String, ChatRoomPane> rooms;
	
	public RoomTabbedPane(CloseableRoom roomCloseable, UserObtainable obtainable, Sendeable sender, Dictionable dictionary) {
		this.roomCloseable = roomCloseable;
		this.obtainable = obtainable;
		this.sender = sender;
		this.dictionary = dictionary;
		rooms = new Hashtable<String, ChatRoomPane>();
		//createRoom("Hola");
	}
	
	public WritteableChat createRoom(String roomName) {
		ChatRoomPane room = new ChatRoomPane(roomName, obtainable, sender, dictionary);
		add(roomName, room);
		rooms.put(roomName, room);
		int i = indexOfTab(roomName);
		setTabComponentAt(i, new TabComponent(this, roomName));
		return room;
	}
	
	@Override
	public void closeTab(String nameTab) {
		int i = indexOfTab(nameTab);
		remove(i);
		rooms.remove(nameTab).stopRefresher();
		roomCloseable.closeRoom(nameTab);
		revalidate();
		repaint();
	}
	
}
