package client.ui.components;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import server.launcher.Launcher;
import client.ui.exceptions.ConnectionRefussedException;
import client.ui.exceptions.UserAlreadyExistsException;
import client.ui.interfaces.ConnectionOpenable;

public class DialogLogIn extends JDialog {
	
	private ConnectionOpenable openable;
	private JTextField host;
	private JTextField port;
	
	private boolean closed;
	
	public DialogLogIn(ConnectionOpenable openable) {
		this.openable = openable;
		closed = false;
		addWindowListener(new WindowCloseEvent());
		
		setModal(true);
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new GridLayout(3, 2));
		setContentPane(contentPane);
		
		JLabel labelHost = new JLabel("IP: ");
		contentPane.add(labelHost); 
		
		host = new JTextField("localhost");
		contentPane.add(host);
		
		JLabel labelPort = new JLabel("Port: ");
		contentPane.add(labelPort);
		
		port = new JTextField();
		port.setText(String.valueOf(Launcher.WELCOME_PORT));
		contentPane.add(port);
		
		JButton buttonOk = new JButton("Acceptar");
		contentPane.add(buttonOk);
		buttonOk.addActionListener(new AcceptEvent());
		
		JButton buttonCancel = new JButton("Cancel·lar");
		contentPane.add(buttonCancel);
		buttonCancel.addActionListener(new CancelEvent());
		
		pack();
	}
	
	public boolean hasBeenClosed() {
		return closed;
	}
	
	private class AcceptEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				openable.openConnection(host.getText(), Integer.parseInt(port.getText()));
				JTextArea area = new JTextArea();
				int okCxl = JOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor(DialogLogIn.this),
						area, "Introdueïx usuari:", JOptionPane.OK_CANCEL_OPTION);
				if (okCxl == JOptionPane.OK_OPTION) {
					String userName = area.getText();
					openable.logInServer(userName);
					setVisible(false);
				}
			} catch (NumberFormatException ee) {
				JOptionPane.showMessageDialog(null, "Error , el port deu ser numéric", "Error", JOptionPane.ERROR_MESSAGE);
			} catch (ConnectionRefussedException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} catch (UserAlreadyExistsException e1) {
				JOptionPane.showMessageDialog(null, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
	}
	private class CancelEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			closed = true;
			setVisible(false);
		}
		
	}
	private class WindowCloseEvent extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			closed = true;
		}
	}
}
