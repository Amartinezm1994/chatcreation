package client.ui.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import chat.utils.Dictionable;
import client.ui.events.ActionSendMessage;
import client.ui.events.RefreshingUsersThread;
import client.ui.interfaces.Sendeable;
import client.ui.interfaces.UserObtainable;
import client.ui.interfaces.UserRefresheable;
import client.ui.interfaces.WritteableChat;
import server.exceptions.MalformedDictionaryException;

public class ChatRoomPane extends JPanel implements WritteableChat, UserRefresheable {
	
	private String tabName;
	private JTextArea textArea;
	private JTextArea userList;
	private JTextField writeField;
	private JScrollPane scrollText;
	private RefreshingUsersThread refresher;
	private ActionSendMessage actionSend;
	
	public ChatRoomPane(String tabName, UserObtainable obtainable, Sendeable sender, Dictionable dictionary) {
		this.tabName = tabName;
		
		GridBagLayout gbl = new GridBagLayout();
		gbl.columnWeights = new double[] {1, 0.3};
		gbl.rowWeights = new double[] {1.0};
		setLayout(gbl);
		
		JPanel chatPane = new JPanel();
		GridBagConstraints gbcChat = new GridBagConstraints();
		gbcChat.fill = GridBagConstraints.BOTH;
		gbcChat.gridx = 0;
		add(chatPane, gbcChat);
		chatPane.setLayout(new BorderLayout());
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		scrollText = new JScrollPane(textArea);
		chatPane.add(scrollText, BorderLayout.CENTER);
		
		writeField = new JTextField();
		chatPane.add(writeField, BorderLayout.PAGE_END);
		
		userList = new JTextArea();
		userList.setEditable(false);
		
		JScrollPane scroll = new JScrollPane(userList);
		GridBagConstraints gbcTable = new GridBagConstraints();
		gbcTable.fill = GridBagConstraints.BOTH;
		gbcTable.gridx = 1;
		add(scroll, gbcTable);
		
		refresher = new RefreshingUsersThread(this, obtainable, dictionary);
		refresher.start();
		
		actionSend = new ActionSendMessage(sender, tabName);
		
		writeField.addKeyListener(new KeyListenerField());
		
	}
	
	public void stopRefresher() {
		refresher.stopRefreshing();
	}

	@Override
	public void writteMessage(String message) {
		textArea.setText(textArea.getText() + "\n" + message);
		scrollText.getVerticalScrollBar().setValue(scrollText.getVerticalScrollBar().getMaximum());
	}

	@Override
	public void refreshUsers(String users) {
		userList.setText(users);
	}

	@Override
	public String getRoomName() {
		return tabName;
	}
	
	private class KeyListenerField extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			super.keyPressed(e);
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				String text = writeField.getText();
				actionSend.sendMessage(text);
				if (actionSend.doAction()) {
					writteMessage("Tú: " + text);
				} else {
					writteMessage("Error en l'enviament.");
				}
				writeField.setText("");
			}
		}
	}
}
