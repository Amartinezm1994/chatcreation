package client.ui.components;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.ui.interfaces.RoomCreable;

public class DialogCreateRoom extends JDialog {
	
	private JTextField roomName;
	private RoomCreable creable;
	
	public DialogCreateRoom(RoomCreable creable) {
		this.creable = creable;
		
		setModal(true);
		
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new GridLayout(2,2));
		setContentPane(contentPane);
		
		JLabel labelNameRoom = new JLabel("Nom de la sala: ");
		contentPane.add(labelNameRoom);
		
		roomName = new JTextField();
		contentPane.add(roomName);
		
		JButton buttonOk = new JButton("Acceptar");
		contentPane.add(buttonOk);
		buttonOk.addActionListener(new ActionButtonOk());
		
		JButton buttonCancel = new JButton("Cancel·lar");
		contentPane.add(buttonCancel);
		buttonCancel.addActionListener(new ActionButtonCancel());
		
		pack();
	}
	
	private class ActionButtonOk implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			creable.createRoom(roomName.getText());
			dispose();
		}
		
	}
	private class ActionButtonCancel implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
		
	}
}
