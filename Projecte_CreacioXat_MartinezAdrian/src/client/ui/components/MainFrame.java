package client.ui.components;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import server.exceptions.MalformedDictionaryException;
import client.core.classes.ClientCore;
import client.core.interfaces.Loggeable;
import client.core.interfaces.Responseable;
import client.core.interfaces.Roomeable;
import client.ui.events.ActionCreateRoom;
import client.ui.events.ActionLeaveRoom;
import client.ui.events.ActionLogin;
import client.ui.exceptions.ConnectionRefussedException;
import client.ui.exceptions.UserAlreadyExistsException;
import client.ui.interfaces.CloseableRoom;
import client.ui.interfaces.ConnectionOpenable;
import client.ui.interfaces.RoomCreable;
import client.ui.interfaces.RoomJoineable;
import client.ui.interfaces.Sendeable;
import client.ui.interfaces.UserObtainable;

public class MainFrame extends JFrame implements ConnectionOpenable, 
		CloseableRoom, UserObtainable, Sendeable, RoomCreable,
		RoomJoineable {
	
	private ClientCore client;
	private Loggeable login;
	private Roomeable roomeable;
	private JPanel contentPane;
	private RoomTabbedPane roomPane;
	
	public MainFrame() {
		addWindowListener(new WindowCloseEvent());
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		contentPane = new JPanel();
		
		setContentPane(contentPane);
		
		JButton buttonToLog = new JButton("Connectar!");
		contentPane.add(buttonToLog);
		buttonToLog.addActionListener(new LogInButtonEvent());
		
		pack();
	}
	
	private void constructContentPane() {
		contentPane.removeAll();
		
		BorderLayout bLayout = new BorderLayout();
		
		contentPane.setLayout(bLayout);
		
		JMenuBar menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.PAGE_START);
		
		JMenu optionBar = new JMenu("Chat");
		menuBar.add(optionBar);
		
		JMenuItem joinChat = new JMenuItem("Join chat");
		optionBar.add(joinChat);
		joinChat.addActionListener(new ActionOpenDialogJoinRoom());
		
		JMenuItem createChat = new JMenuItem("Create chat");
		optionBar.add(createChat);
		createChat.addActionListener(new ActionOpenDialogCreateRoom());
		
		roomPane = new RoomTabbedPane(this, this, this, client);
		contentPane.add(roomPane, BorderLayout.CENTER);

		setSize(400, 400);
		
		revalidate();
		repaint();
	}
	
	private void closeAll() {
		if (client != null)
			client.close();
		dispose();
		System.exit(0);
	}

	@Override
	public void openConnection(String host, int port)
			throws ConnectionRefussedException {
		try {
			client = new ClientCore(host, port);
			client.init();
		} catch (MalformedDictionaryException | NoSuchAlgorithmException e) {
			JOptionPane.showMessageDialog(null, "Error amb el diccionari intern.", "Error", JOptionPane.ERROR_MESSAGE);
			closeAll();
		} catch (UnknownHostException e) {
			throw new ConnectionRefussedException("Adreça ip no trobada.");
		} catch (IOException e) {
			throw new ConnectionRefussedException("Error amb la connexió.");
		}
		login = client.getLogin();
		roomeable = client.getRoomeable();
	}
	
	@Override
	public void obtainUsers(String nameRoom, Responseable resp) {
		roomeable.recuperateUsers(nameRoom, resp);
	}

	@Override
	public void logInServer(String userName) throws UserAlreadyExistsException {
		login.logIntoServer(userName, new ActionLogin());
	}
	

	@Override
	public void closeRoom(String roomName) {
		roomeable.leaveRoom(roomName, new ActionLeaveRoom());
	}
	
	

	@Override
	public void sendMessage(String message, String roomName, Responseable resp) {
		roomeable.sendMessage(message, roomName, resp);
	}

	@Override
	public void createRoom(String roomName) {
		roomeable.createRoom(roomName, new ActionCreateRoom(this, roomName), roomPane.createRoom(roomName));
	}

	@Override
	public void joinRoom(String roomName) {
		roomeable.joinRoom(roomName, new ActionCreateRoom(this, roomName), roomPane.createRoom(roomName));
	}

	private class LogInButtonEvent implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			DialogLogIn dialogToLogin = new DialogLogIn(MainFrame.this);
			dialogToLogin.setVisible(true);
			if (!dialogToLogin.hasBeenClosed()) {
				constructContentPane();
			}
		}
	}
	
	private class ActionOpenDialogCreateRoom implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			DialogCreateRoom dialogCreateRoom = new DialogCreateRoom(MainFrame.this);
			dialogCreateRoom.setVisible(true);
		}
	}
	
	private class ActionOpenDialogJoinRoom implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			DialogSelectRoom dialogRoom = new DialogSelectRoom(MainFrame.this, client);
			roomeable.getRooms(dialogRoom);
		}
	}
	
	private class WindowCloseEvent extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			super.windowClosing(e);
			closeAll();
		}
	}
}
