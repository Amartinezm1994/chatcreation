package client.ui.events;

import client.core.interfaces.Responseable;

public class ActionLeaveRoom implements Responseable {
	
	private boolean waiting;
	private boolean doAction;
	private String response;
	
	public ActionLeaveRoom() {
		waiting = true;
		doAction = false;
	}

	@Override
	public void setResponse(boolean state, String response) {
		this.response = response;
		waiting = false;
		doAction = state;
	}

	@Override
	public String getReasonOfThrow() {
		return response;
	}

	@Override
	public boolean waiting() {
		return waiting;
	}

	@Override
	public boolean doAction() {
		return doAction;
	}
	
}
