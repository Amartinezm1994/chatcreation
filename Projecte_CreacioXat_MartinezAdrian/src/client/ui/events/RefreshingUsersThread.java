package client.ui.events;

import chat.utils.Dictionable;
import client.core.interfaces.Responseable;
import client.ui.interfaces.UserObtainable;
import client.ui.interfaces.UserRefresheable;

public class RefreshingUsersThread extends Thread implements Responseable {
	
	private UserRefresheable refresheable;
	private UserObtainable obtainable;
	private Dictionable dictionary;
	private boolean waiting;
	private String response;
	
	private boolean alive;
	private static final int REFRESH_RATIO = 2000;
	
	public RefreshingUsersThread(UserRefresheable refresheable, UserObtainable obtainable, Dictionable dictionary) {
		this.refresheable = refresheable;
		this.dictionary = dictionary;
		this.obtainable = obtainable;
		waiting = true;
		alive = false;
	}
	@Override
	public synchronized void start() {
		alive = true;
		super.start();
	}
	@Override
	public void run() {
		super.run();
		while (alive) {
			try {
				sleep(REFRESH_RATIO);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			obtainable.obtainUsers(refresheable.getRoomName(), this);
		}
	}
	public void stopRefreshing() {
		alive = false;
		synchronized(this){
			notifyAll();
		}
	}
	@Override
	public void setResponse(boolean state, String response) {
		String users = null;
		if (state) {
			users = response.replaceAll(dictionary.getInternalSeparator(), "\n");
		} else {
			this.response = response;
			users = response;
		}
		waiting = false;
		refresheable.refreshUsers(users);
	}

	@Override
	public String getReasonOfThrow() {
		return response;
	}

	@Override
	public boolean waiting() {
		return waiting;
	}

	@Override
	public boolean doAction() {
		return true;
	}

}
