package client.ui.events;

import client.core.interfaces.Responseable;

public class ActionLogin implements Responseable {
	
	private boolean waiting;
	private boolean doAction;
	private String reasonError;
	
	public ActionLogin() {
		waiting = true;
		doAction = false;
	}
	
	@Override
	public void setResponse(boolean state, String response) {
		if (!state)
			reasonError = response;
		doAction = state;
		waiting = false;
	}

	@Override
	public boolean waiting() {
		return waiting;
	}

	@Override
	public boolean doAction() {
		return doAction;
	}

	@Override
	public String getReasonOfThrow() {
		return reasonError;
	}
	
}
