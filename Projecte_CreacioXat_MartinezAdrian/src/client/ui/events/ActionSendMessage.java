package client.ui.events;

import client.core.interfaces.Responseable;
import client.ui.interfaces.Sendeable;

public class ActionSendMessage implements Responseable {
	
	private Sendeable sender;
	private String roomName;
	
	private boolean waiting;
	private String response;
	private boolean state;
	
	public ActionSendMessage(Sendeable sender, String roomName) {
		this.sender = sender;
		this.roomName = roomName;
	}
	
	public void sendMessage(String message) {
		waiting = true;
		sender.sendMessage(message, roomName, this);
	}

	@Override
	public void setResponse(boolean state, String response) {
		this.response = response;
		this.state = state;
		waiting = false;
	}

	@Override
	public String getReasonOfThrow() {
		return response;
	}

	@Override
	public boolean waiting() {
		return waiting;
	}

	@Override
	public boolean doAction() {
		return state;
	}
	
}
