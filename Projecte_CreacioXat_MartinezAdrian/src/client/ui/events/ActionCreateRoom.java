package client.ui.events;

import client.core.interfaces.Responseable;
import client.ui.interfaces.CloseableRoom;

public class ActionCreateRoom implements Responseable {
			
	private boolean waiting;
	private boolean doAction;
	private CloseableRoom closeable;
	private String roomName;
	
	public ActionCreateRoom(CloseableRoom closeable, String roomName) {
		waiting = true;
		this.closeable = closeable;
	}
	
	@Override
	public void setResponse(boolean state, String response) {
		doAction = state;
		waiting = false;
		if (!state) {
			closeable.closeRoom(roomName);
		}
	}

	@Override
	public String getReasonOfThrow() {
		return null;
	}

	@Override
	public boolean waiting() {
		return waiting;
	}

	@Override
	public boolean doAction() {
		return doAction;
	}
	
}
