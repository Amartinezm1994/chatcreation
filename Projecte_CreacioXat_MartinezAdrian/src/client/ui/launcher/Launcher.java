package client.ui.launcher;

import client.ui.components.MainFrame;

public class Launcher {
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                MainFrame window = new MainFrame();
				window.setVisible(true);
            }
        });
	}
}
