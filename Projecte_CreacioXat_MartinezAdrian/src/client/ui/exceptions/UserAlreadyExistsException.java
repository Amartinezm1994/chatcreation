package client.ui.exceptions;

public class UserAlreadyExistsException extends Exception {
	public UserAlreadyExistsException(String msg) {
		super(msg);
	}
	public UserAlreadyExistsException() {
	}
}
