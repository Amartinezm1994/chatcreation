package client.ui.interfaces;

public interface RoomJoineable {
	public void joinRoom(String roomName);
}
