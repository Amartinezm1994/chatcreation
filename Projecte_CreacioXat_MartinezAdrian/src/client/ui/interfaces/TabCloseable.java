package client.ui.interfaces;

public interface TabCloseable {
	public void closeTab(String nameTab);
}
