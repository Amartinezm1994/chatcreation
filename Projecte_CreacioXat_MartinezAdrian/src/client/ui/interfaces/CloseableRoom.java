package client.ui.interfaces;

public interface CloseableRoom {
	public void closeRoom(String roomName);
}
