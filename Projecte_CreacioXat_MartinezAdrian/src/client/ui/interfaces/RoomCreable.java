package client.ui.interfaces;

public interface RoomCreable {
	public void createRoom(String roomName);
}
