package client.ui.interfaces;

import client.core.interfaces.Responseable;

public interface UserObtainable {
	public void obtainUsers(String nameRoom, Responseable resp);
}
