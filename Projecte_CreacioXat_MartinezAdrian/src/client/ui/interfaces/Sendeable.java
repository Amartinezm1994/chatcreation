package client.ui.interfaces;

import client.core.interfaces.Responseable;

public interface Sendeable {
	public void sendMessage(String message, String roomName, Responseable resp);
}
