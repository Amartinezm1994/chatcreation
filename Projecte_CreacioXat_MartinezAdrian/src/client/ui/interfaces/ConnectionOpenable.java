package client.ui.interfaces;

import client.ui.exceptions.ConnectionRefussedException;
import client.ui.exceptions.UserAlreadyExistsException;

public interface ConnectionOpenable {
	public void openConnection(String host, int port) throws ConnectionRefussedException;
	public void logInServer(String userName) throws UserAlreadyExistsException;
}
