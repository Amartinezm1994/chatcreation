package client.ui.interfaces;

import java.util.Vector;

public interface UserRefresheable {
	public void refreshUsers(String users);
	public String getRoomName();
}
