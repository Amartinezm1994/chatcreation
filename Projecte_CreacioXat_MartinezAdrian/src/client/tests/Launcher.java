package client.tests;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;


public class Launcher {
	public static void main(String[] args) throws UnknownHostException, IOException {
		String host = "localhost";
		int port = 6000;
		
		Socket sock = new Socket(InetAddress.getByName(host), port);
		InputStream inp = sock.getInputStream();
		DataInputStream dataInp = new DataInputStream(inp);
		
		OutputStream out = sock.getOutputStream();
		DataOutputStream dataOut = new DataOutputStream(out);
		
		Thread thrSender = new Thread(new Runnable() {

			@Override
			public void run() {
				Scanner sc = new Scanner(System.in);
				try {
					while (true) {
						System.out.print("Escriu: ");
						dataOut.writeUTF(sc.next());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		});
		
		Thread thrGetter = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					while (true) {
						System.out.println("Missatge rebut: " + dataInp.readUTF());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		});
		thrSender.start();
		thrGetter.start();
	}
}
