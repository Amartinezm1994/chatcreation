package client.core.classes;
import java.io.IOException;

import client.ui.interfaces.WritteableChat;


public class ChatRoom/* extends Thread */{
	private String roomName;
	private WritteableChat writter;
	//private Vector<String> connectedUsers;
	
	public ChatRoom(String roomName, WritteableChat writter) {
		this.roomName = roomName;
		this.writter = writter;
	}
	
	public String getRoomName() {
		return roomName;
	}
	
	/*public void setUserList(Vector<String> users) {
		connectedUsers = users;
	}
	public void setUserList(String[] users) {
		connectedUsers = new Vector<String>();
		for (String user : users) {
			connectedUsers.add(user);
		}
	}*/
	
	public void writeMessage(String userName, String message) throws IOException {
		String formattedMessage = userName + ": " + message;
		//toSend.writeUTF(formattedMessage);
		writter.writteMessage(formattedMessage);
	}
}
