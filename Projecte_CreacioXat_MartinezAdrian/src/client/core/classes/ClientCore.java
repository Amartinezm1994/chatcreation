package client.core.classes;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Vector;

import client.core.interfaces.Loggeable;
import client.core.interfaces.Queueable;
import client.core.interfaces.Responseable;
import client.core.interfaces.RoomGestionable;
import client.core.interfaces.Roomeable;
import chat.utils.Dictionable;
import chat.utils.ProtocolDictionary;
import server.exceptions.MalformedDictionaryException;


public class ClientCore implements RoomGestionable, Queueable, Dictionable {
	
	private Socket sockClient;
	private DataInputStream input;
	private DataOutputStream output;
	private int port;
	private InetAddress ipAddress;
	private ProtocolDictionary dictionary;
	private Hashtable<String, ChatRoom> rooms;
	private ClientChat chatClient;
	
	private Queue<Responseable> responses;
	private List<ReceiverClass> receivers;
	
	private boolean running;
	private boolean reading;
	
	private static final int INTERNAL_RECEIVERS = 10;
	private static final int TIMEOUT_RECEIVERS = 2000;
	
	public ClientCore(String ipAddress, int port) 
			throws UnknownHostException, IOException, NoSuchAlgorithmException, MalformedDictionaryException {
		this.ipAddress = InetAddress.getByName(ipAddress);
		this.port = port;
		dictionary = ProtocolDictionary.getInstance();
		chatClient = new ClientChat(this, dictionary, this);
	}
	
	public void init() throws IOException {
		sockClient = new Socket(ipAddress, port);
		sockClient.setSoTimeout(TIMEOUT_RECEIVERS);
		input = new DataInputStream(sockClient.getInputStream());
		output = new DataOutputStream(sockClient.getOutputStream());
		rooms = new Hashtable<String, ChatRoom>();
		responses = new PriorityQueue<Responseable>();
		running = true;
		reading = false;
		receivers = new Vector<ReceiverClass>();
		for (int i = 0; i < INTERNAL_RECEIVERS; i++) {
			ReceiverClass receiver = new ReceiverClass();
			receiver.start();
			receivers.add(receiver);
		}
	}
	
	public boolean isConnected() {
		if (sockClient != null)
			return sockClient.isConnected();
		return false;
	}
	
	public void close() {
		if (running)
			running = false;
		try {
			if (sockClient.isConnected()) {
				input.close();
				output.close();
				sockClient.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Roomeable getRoomeable() {
		return chatClient;
	}
	public Loggeable getLogin() {
		return chatClient;
	}
	
	private void receive() throws IOException {
		String message = null;
		synchronized (this) {
			message = input.readUTF();
		}
		analyzeReceivedMessage(message);
	}
	
	private void analyzeReceivedMessage(String message) {
		String separator = dictionary.getWord(ProtocolDictionary.CODE_SPLIT);
		String okCode = dictionary.getWord(ProtocolDictionary.CODE_OK);
		String errCode = dictionary.getWord(ProtocolDictionary.CODE_ERROR);
		String[] splittedMessage = message.split(separator);
		if (splittedMessage[0].equals(okCode)) {
			Responseable resp = responses.remove();
			String response = null;
			try {
				response = splittedMessage[1];
			} catch (IndexOutOfBoundsException e) {
				response = "OK";
			}
			resp.setResponse(true, response);
			synchronized (resp) {
				resp.notifyAll();
			}
			
		} else if (splittedMessage[0].equals(errCode)) {
			Responseable resp = responses.remove();
			String response = null;
			try {
				response = splittedMessage[1];
			} catch (IndexOutOfBoundsException e) {
				response = "ERROR";
			}
			resp.setResponse(false, response);
			synchronized (resp) {
				resp.notifyAll();
			}
			
		} else {
			String internalSeparator = dictionary.getWord(ProtocolDictionary.CODE_INTERNALSEPARATOR);
			String[] arrayRoom = splittedMessage[0].split(internalSeparator);
			String roomName = arrayRoom[0];
			String userSend = arrayRoom[1];
			String messageSend = arrayRoom[2];
			ChatRoom room = rooms.get(roomName);
			try {
				room.writeMessage(userSend, messageSend);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void send(String message) throws IOException {
		//synchronized(this) {
		//System.out.println(message);
		reading = true;
		output.writeUTF(message);
		reading = false;
		synchronized(this){
			notifyAll();
		}
		//}
	}
	
	@Override
	public void addRoom(ChatRoom room) {
		rooms.put(room.getRoomName(), room);
	}

	@Override
	public void removeRoom(String nameRoom) {
		rooms.remove(nameRoom);
	}

	@Override
	public void addToSendQueue(String protocolizedMessage) {
		SenderClass sender = new SenderClass(protocolizedMessage);
		sender.start();
	}

	@Override
	public void addToResponseQueue(Responseable response) {
		responses.add(response);
	}
	
	@Override
	public String getInternalSeparator() {
		return dictionary.getWord(ProtocolDictionary.CODE_INTERNALSEPARATOR);
	}
	
	private class SenderClass extends Thread {
		private String message;
		public SenderClass(String message) {
			this.message = message;
		}
		@Override
		public void run() {
			super.run();
			try {
				send(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	private class ReceiverClass extends Thread {
		@Override
		public void run() {
			super.run();
			while (running) {
				try {
					receive();
				} catch (SocketTimeoutException e) {
					// Do nothing
				} catch (EOFException e) {
					while (reading)
						try {
							synchronized(this){
								wait();
							}
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
				} catch (SocketException e) {
					close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	
}
