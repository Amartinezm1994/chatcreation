package client.core.classes;

import chat.utils.ProtocolDictionary;
import client.core.interfaces.Loggeable;
import client.core.interfaces.Queueable;
import client.core.interfaces.Responseable;
import client.core.interfaces.RoomGestionable;
import client.core.interfaces.Roomeable;
import client.ui.exceptions.UserAlreadyExistsException;
import client.ui.interfaces.WritteableChat;

class ClientChat implements Roomeable, Loggeable {
	
	private Queueable queue;
	private ProtocolDictionary dictionary;
	private RoomGestionable gestor;
	
	public ClientChat(Queueable queue, ProtocolDictionary dictionary, RoomGestionable gestor) {
		this.queue = queue;
		this.dictionary = dictionary;
		this.gestor = gestor;
	}
	
	@Override
	public void getRooms(Responseable resp) {
		String protocolizedMessage = dictionary.getWord(ProtocolDictionary.CODE_GETROOMS);
		queue.addToSendQueue(protocolizedMessage);
		queue.addToResponseQueue(resp);
	}
	@Override
	public void recuperateUsers(String nameRoom, Responseable resp) {
		String separator = dictionary.getWord(ProtocolDictionary.CODE_SPLIT);
		String protocolizedMessage = dictionary.getWord(ProtocolDictionary.CODE_GETUSERSROOM) 
				+ separator + nameRoom;
		queue.addToSendQueue(protocolizedMessage);
		queue.addToResponseQueue(resp);
	}
	@Override
 	public void leaveRoom(String nameRoom, Responseable resp) {
		String separator = dictionary.getWord(ProtocolDictionary.CODE_SPLIT);
		String protocolizedMessage = dictionary.getWord(ProtocolDictionary.CODE_LEAVEROOM) 
				+ separator + nameRoom;
		queue.addToSendQueue(protocolizedMessage);
		queue.addToResponseQueue(resp);
		while (resp.waiting()) {
			synchronized (resp) {
				try {
					resp.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		if (resp.doAction()) {
			gestor.removeRoom(nameRoom);
		}
	}
 	@Override
	public void createRoom(String nameRoom, Responseable resp, WritteableChat writter) {
		String separator = dictionary.getWord(ProtocolDictionary.CODE_SPLIT);
		String protocolizedMessage = dictionary.getWord(ProtocolDictionary.CODE_CREATEROOM) 
				+ separator + nameRoom;
		managementRoom(protocolizedMessage, resp, writter, nameRoom);
	}
	@Override
	public void joinRoom(String nameRoom, Responseable resp, WritteableChat writter) {
		String separator = dictionary.getWord(ProtocolDictionary.CODE_SPLIT);
		String protocolizedMessage = dictionary.getWord(ProtocolDictionary.CODE_JOINROOM)
				+ separator + nameRoom;
		managementRoom(protocolizedMessage, resp, writter, nameRoom);
	}
	private void managementRoom(String protocolizedMessage, 
			Responseable resp, WritteableChat writter, String nameRoom) {
		queue.addToSendQueue(protocolizedMessage);
		queue.addToResponseQueue(resp);
		while (resp.waiting()) {
			synchronized (resp) {
				try {
					resp.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		if (resp.doAction()) {
			ChatRoom room = new ChatRoom(nameRoom, writter);
			gestor.addRoom(room);
		}
	}
	
	@Override
	public void sendMessage(String message, String roomName, Responseable resp) {
		String separator = dictionary.getWord(ProtocolDictionary.CODE_SPLIT);
		String protocolizedMessage = dictionary.getWord(ProtocolDictionary.CODE_SENDMESSAGE) 
				+ separator + roomName + separator + message;
		queue.addToSendQueue(protocolizedMessage);
		queue.addToResponseQueue(resp);
		while (resp.waiting()) {
			synchronized (resp) {
				try {
					resp.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}


	@Override
	public void logIntoServer(String userName, Responseable resp) throws UserAlreadyExistsException {
		queue.addToSendQueue(userName);
		queue.addToResponseQueue(resp);
		while (resp.waiting()) {
			synchronized (resp) {
				try {
					resp.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		if (!resp.doAction())
			throw new UserAlreadyExistsException(resp.getReasonOfThrow());
	}
}
