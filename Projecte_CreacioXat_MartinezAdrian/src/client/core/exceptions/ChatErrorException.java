package client.core.exceptions;

public class ChatErrorException extends Exception {
	public ChatErrorException(String msg) {
		super(msg);
	}
	public ChatErrorException() {
	}
}
