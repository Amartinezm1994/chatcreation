package client.core.interfaces;

import client.ui.interfaces.WritteableChat;


public interface Roomeable {
	public void sendMessage(String message, String roomName, Responseable resp);
	public void joinRoom(String nameRoom, Responseable resp, WritteableChat writter);
	public void createRoom(String nameRoom, Responseable resp, WritteableChat writter);
	public void getRooms(Responseable resp);
	public void recuperateUsers(String nameRoom, Responseable resp);
	public void leaveRoom(String nameRoom, Responseable resp);
}
