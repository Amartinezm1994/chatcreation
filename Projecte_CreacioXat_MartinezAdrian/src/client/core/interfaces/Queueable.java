package client.core.interfaces;

public interface Queueable {
	public void addToSendQueue(String protocolizedMessage);
	public void addToResponseQueue(Responseable response);
}
