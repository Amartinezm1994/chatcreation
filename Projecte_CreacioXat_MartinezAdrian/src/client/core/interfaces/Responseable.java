package client.core.interfaces;

public interface Responseable {
	public void setResponse(boolean state, String response);
	public String getReasonOfThrow();
	public boolean waiting();
	public boolean doAction();
}
