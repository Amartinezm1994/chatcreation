package client.core.interfaces;

import client.ui.exceptions.UserAlreadyExistsException;

public interface Loggeable {
	public void logIntoServer(String userName, Responseable resp) throws UserAlreadyExistsException ;
}
