package client.core.interfaces;

import client.core.classes.ChatRoom;

public interface RoomGestionable {
	public void addRoom(ChatRoom room);
	public void removeRoom(String nameRoom);
}
